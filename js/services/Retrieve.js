app.factory('Retrieve', ['$http', function($http) {
	return {
		exec: function() {
			return $http.get('http://localhost/lab/angular/retrieve_service.php')
				.success(function(data) {
					return data;
				})
				.error(function() {
					return false;
				});
		}
	};
}]);