app.factory('Update', ['$http', function($http) {
	return {
		exec: function(id, column) {
			return $http.get('http://localhost/lab/angular/update_service.php?id='+id+'&column='+column)
				.success(function() {
					return true;
				})
				.error(function() {
					return false;
				});
		}
	};
}]);