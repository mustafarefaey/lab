 app.controller('MainController', ['$scope', 'Update', 'Retrieve', function($scope, Update, Retrieve) {
  $scope.like = function(index) {
  	Update.exec(index+1, 'likes').success(function(data){
  		$scope.products[index].likes += 1;
  	});
  };
  $scope.dislike = function(index) {
  	Update.exec(index+1, 'dislikes').success(function(data){
  		$scope.products[index].dislikes += 1;
  	});
  };

  Retrieve.exec().success(function(data){
  	$scope.products = data;
  	angular.forEach($scope.products, function(value, key) {
  		$scope.products[key]['likes'] = parseInt($scope.products[key]['likes'], 10);
  		$scope.products[key]['dislikes'] = parseInt($scope.products[key]['dislikes'], 10);
  	});
  });

}]);